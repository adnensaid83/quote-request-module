# Module de demande de devis interactif

![adnen](src/assets/banner.png)

# Description

Mon module de demande de devis simplifie le processus de communication entre les agences et leurs clients potentiels. Conçu sous la forme d'un stepper interactif hautement personnalisable, il offre aux clients la flexibilité de choisir parmi différents services, de répondre à des questions spécifiques avec des options à choix unique et à choix multiples, et enfin, de remplir un formulaire de contact pour une communication ultérieure.
Une fois les demandes envoyées, elles sont instantanément acheminées vers votre compte DatoCMS, un headless CMS fiable. Pour configurer votre compte et commencer à gérer efficacement vos demandes, veuillez suivre les instructions détaillées ci-dessous.

# Languages utilisés

- REACTJS, TYPESCRIPT, FORMIZ, Vite
- TAILWINDCSS
- axios
- DATOCMS(headlessCMS), GRAPHQL

# Responsive

Oui, il est responsive

# Comment ça marche

1. **Installation des dépendances:** Exécutez la commande `npm install` pour installer toutes les dépendances nécessaires.
2. **création d'un compte DatoCMS:** Rendez-vous sur [DatoCMS Dashboard](https://dashboard.datocms.com/signup) et créez un compte.
   Une fois connecté, créez un nouveau projet en choisissant un nom.
3. **Configuration des Modèles dans DatoCMS :**

- Créez un modèle "Option" en cliquant sur "Create new model" en bas de la page. Ajoutez toutes les fields nécessaires comme sur la photo ci-dessous.
  ![option](src/assets/Option.png)
- Ajouter toutes les options nécessaires.
- Créez un modèle "Question" et ajoutez des questions avec les options correspondantes.
  ![question](src/assets/question.png)
- Créez un modèle "Service" et ajoutez des services avec les questions correspondantes.
  ![service](src/assets/service.png)
- Créez un modèle "Estimate" pour représenter les devis.
  ![estimate](src/assets/estimate.png)

4. **Configuration du Fichier .env:** Créez un fichier .env et ajoutez les informations suivantes :
   `VITE_API_TOKEN=VotreToken`
   `VITE_API_URL=https://graphql.datocms.com/`
   `VITE_ESTIMATE_ID=VotreIdEstimate`
   ![token](src/assets/apiToken.png)
   ![idDevis](src/assets/idEstimate.png)

5. **Finalisation:** Exécutez la commande `npm run dev`
   Vous êtes prêt à utiliser le module de demande de devis

# UI

![step1](src/assets/step1.png)
![step2](src/assets/step2.png)
![step3](src/assets/step3.png)
![step4](src/assets/step4.png)

Datocms

![dashboard1](src/assets/datocms1.png)
![dashboard2](src/assets/datocms2.png)
