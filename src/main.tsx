import React, { lazy } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import Root from "./routes/root";
import ErrorPage from "./error-page";
import ServiceDetail from "./routes/ServiceDetail";
import "vite/modulepreload-polyfill";
import Loader from "./components/loader";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import Home from "./routes/Home";
const ServicesPage = lazy(() => import("./routes/servicesPage"));
//const Home = lazy(() => import("./routes/homePage/Home"));
const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Root />} errorElement={<ErrorPage />}>
      <Route index element={<Home />} />
      <Route
        path="services/*"
        element={
          <React.Suspense fallback={<Loader />}>
            <ServicesPage />
          </React.Suspense>
        }
      />

      <Route path="services/:slug" element={<ServiceDetail />} />
    </Route>
  )
);
const client = new ApolloClient({
  uri: import.meta.env.VITE_API_URL,
  cache: new InMemoryCache(),
  headers: {
    Authorization: `Bearer ${import.meta.env.VITE_API_TOKEN} `,
  },
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <RouterProvider router={router} fallbackElement={<Loader />} />
    </ApolloProvider>
  </React.StrictMode>
);
