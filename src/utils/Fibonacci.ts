export function fibonacci(n: number) {
  let a = 0;
  let b = 1;
  const sequence = [];

  for (let i = 1; i <= n; i++) {
    sequence.unshift(a);
    const temp = b;
    b = a + b;
    a = temp;
  }
  return sequence.join(" ");
}
