import { ReactNode } from "react";
import Breadcrump from "./Breadcrumbs";
import { Container } from "./Container";
import { Underline } from "./TitleSection";

export default function Layout({
  title,
  tag,
  children,
}: {
  title: string;
  tag?: string;
  children: ReactNode;
}) {
  return (
    <div className="fex-1">
      <div className="flex flex-col px-12 py-16 items-center bg-green md:py-20 md:px-0">
        <p className="text-base font-light uppercase">{tag}</p>
        <h1 className="text-center flex flex-col gap-6 items-center text-4xl md:text-5xl">
          {title} <Underline />
        </h1>
      </div>
      <Container>
        <div className="px-12 lg:px-0">
          <Breadcrump />
          {children}
        </div>
      </Container>
    </div>
  );
}
