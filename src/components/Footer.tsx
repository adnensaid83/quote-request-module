import { gql, useQuery } from "@apollo/client";
import { RegularList } from "./RegularList";
import SocialListItem from "./SocialListItem";
import Loader from "./loader";

export function Footer() {
  const { loading, error, data } = useQuery(FOOTER_QUERY);
  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <div className="py-12 md:py-24">
      <div className="max-w-[900px] mx-auto">
        <div className="flex flex-col items-center mb-6">
          <ul className="flex gap-6">
            <RegularList
              items={data.footerContent.social}
              resourceName="social"
              itemComponent={SocialListItem}
            />
          </ul>
        </div>
        <div>
          <p className="text-sm text-center font-medium m-0 line-clamp-3 md:text-sm">
            Conçu et réalisé par Adnen Said.
          </p>
          <p className="text-sm text-center font-medium m-0 line-clamp-3 md:text-sm">
            {data.footerContent.copyright}
          </p>
        </div>
      </div>
    </div>
  );
}
const FOOTER_QUERY = gql`
  query {
    footerContent {
      social {
        title
        url
      }
      copyright
    }
  }
`;
