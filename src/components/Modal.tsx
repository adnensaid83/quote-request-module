import { ReactNode, useContext } from "react";
import { X } from "../assets";
import { ThemeContext } from "../routes/root";

export function Modal({
  onClose,
  children,
}: {
  children: ReactNode;
  onClose: () => void;
}) {
  const theme = useContext(ThemeContext);
  return (
    <div
      className={` fixed z-20 lef-0 top-0 w-full h-full overflow-auto ${
        theme === "light" ? "bg-grayModal" : "bg-grayModal"
      }  `}
      onClick={onClose}
    >
      <div
        className={`mx-auto my-28 w-[260px] text-end rounded-md md:w-[400px] ${
          theme === "light" ? "bg-white" : "bg-dark"
        }`}
        onClick={(e) => e.stopPropagation()}
      >
        <button
          onClick={onClose}
          className={`w-[30px] h-[30px] p-2 m-2 rounded-full ${
            theme === "light"
              ? "bg-green md:hover:text-green md:hover:bg-dark"
              : "bg-greenDark md:hover:text-greenDark md:hover:bg-white"
          } `}
        >
          <X />
        </button>
        {children}
      </div>
    </div>
  );
}
