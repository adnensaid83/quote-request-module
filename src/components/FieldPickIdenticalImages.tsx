import { useEffect, useRef, useState } from "react";

import { FieldProps, useField } from "@formiz/core";

import { FormGroupProps } from "./FormGroup";
import { FormGroupCaptcha } from "./FormGroupCaptcha";

type Value = {
  isIdentical: boolean;
  value: string | null;
  selectedCount: number;
  selectedImages: { value: string; index: number }[];
};

export type FieldPickIdenticalImagesProps<FormattedValue> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps & {
    options: string[];
  };

export const FieldPickIdenticalImages = <FormattedValue = string | null,>(
  props: FieldPickIdenticalImagesProps<FormattedValue>
) => {
  const {
    errorMessage,
    id,
    isRequired,
    setValue,
    value,
    isValid,
    isTouched,
    setIsTouched,
    shouldDisplayError,
    otherProps: { children, label, helper, options, ...rest },
  } = useField(props, {
    formatValue: (v) => v?.value ?? null,
    validations: [
      {
        handler: (_, rawValue) => rawValue?.selectedCount === 2,
        message: "You need to select 2 images",
        checkFalsy: true,
      },
      {
        handler: (_, rawValue) => !!rawValue?.isIdentical,
        message: "Image are not identical",
        checkFalsy: true,
      },
    ],
  });

  const formGroupProps = {
    errorMessage,
    helper,
    id,
    isRequired,
    label,
    showError: shouldDisplayError,
    ...rest,
  };

  const selectedImages = value?.selectedImages ?? [];

  const [displayItems, setDisplayItems] = useState<string[]>(() => []);

  const optionsRef = useRef(options);
  optionsRef.current = options;
  useEffect(() => {
    setDisplayItems(
      [...optionsRef.current, ...optionsRef.current].sort(
        () => Math.random() - 0.5
      )
    );
  }, []);

  const handleChange = (itemValue: string, itemIndex: number) => {
    const nextValues = (
      selectedImages.find((x) => x.index === itemIndex)
        ? selectedImages.filter((x) => x.index !== itemIndex)
        : [
            selectedImages[1] || selectedImages[0],
            {
              value: itemValue,
              index: itemIndex,
            },
          ]
    ).filter((x) => !!x);

    const isIdentical =
      !!nextValues[0] &&
      !!nextValues[1] &&
      nextValues[0].value === nextValues[1].value;

    if (!nextValues.length) {
      setValue(null);
      return;
    }

    setIsTouched(nextValues.length > 1);

    setValue({
      isIdentical,
      value: isIdentical ? nextValues[0].value : null,
      selectedCount: nextValues.length,
      selectedImages: nextValues,
    });
  };

  return (
    <FormGroupCaptcha {...formGroupProps}>
      <div className="grid grid-cols-3 sm:grid-cols-6 gap-4 mb-4">
        {displayItems.map((item, index) => (
          <div key={item + index} className="relative aspect-w-1 aspect-h-1">
            <button
              onClick={() => handleChange(item, index)}
              className={`rounded-xl
              focus:outline-none p-0 overflow-hidden text-xs relative
              ${
                selectedImages.find((x) => x.index === index)
                  ? "shadow-outline-blue"
                  : ""
              }
              ${
                selectedImages.length === 2 &&
                selectedImages.every((img) => img.value === item)
                  ? "border-4 border-green200"
                  : ""
              }
              ${selectedImages.length < 2 || isValid ? "text-blue" : "text-red"}
              ${
                selectedImages.length >= 2 &&
                !selectedImages.find((x) => x.index === index)
                  ? "opacity-60"
                  : "opacity-100"
              }
            `}
            >
              <img
                src={item}
                alt={`Image ${index}`}
                className="w-full h-full object-cover"
              />
            </button>
          </div>
        ))}
      </div>
      {isValid && isTouched && (
        <div className="bg-green200 border border-green200 text-white px-4 py-3 rounded-md my-4 flex items-center">
          <svg
            className="h-8 w-8 shrink-0 mr-4 bg-white text-green200 rounded-full"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M5 13l4 4L19 7"
            ></path>
          </svg>
          <p className="m-0">Parfait, les deux images sont identiques !</p>
        </div>
      )}
      {children}
    </FormGroupCaptcha>
  );
};
