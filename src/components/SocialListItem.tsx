import { NavLink } from "react-router-dom";
import { SocialI } from "../interfaces/Social.interface";
import { Gitlab } from "../assets/Gitlab";
import { Instagram, Linkedin } from "../assets";

const SocialListItem = ({ social }: { social: SocialI }) => {
  const { title, url } = social;

  return (
    <li
      className={`w-[36px] h-[36px] flex items-center justify-center border-dark border-[2px] rounded-full p-3 md:p-3 md:hover:text-white md:hover:bg-dark md:hover:border-dark`}
    >
      <NavLink to={url}>
        {title === "Linkedin" ? (
          <Linkedin />
        ) : title === "Instagram" ? (
          <Instagram />
        ) : title === "Gitlab" ? (
          <Gitlab />
        ) : null}
      </NavLink>
    </li>
  );
};

export default SocialListItem;
