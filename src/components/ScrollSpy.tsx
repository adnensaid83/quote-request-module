/* import { ReactNode } from "react";

export default function ScrollSpy({
  nav,
  content,
}: {
  nav: ReactNode;
  content: ReactNode;
}) {
  return (
    <>
      {nav}
      {content}
    </>
  );
} */

import React, { ReactNode, useEffect, useRef, useState } from "react";
import { RegularList } from "./RegularList";

export function ScrollSpy({
  data,
  children,
}: {
  data: any;
  children: ReactNode;
}) {
  const [selected, setSelected] = useState<string | null>(data[0].slug);
  const collapseNavbarMenuRef = useRef<HTMLDivElement | null>(null);
  const sectionsZone = useRef<NodeListOf<HTMLElement> | []>([]);

  const handleCollapseNavbarMenu = () => {
    const collapse = collapseNavbarMenuRef.current;
    collapse?.classList.toggle("hidden");

    // animation nav-item
    allPages.forEach((p: any) => {
      p.nodeRef && p.nodeRef.current?.classList.toggle("item-enter");
    });
  };

  const allPages = data.map((p: any) => ({
    title: p.title,
    slug: p.slug,
    description: p.description,
    nodeRef: React.createRef<HTMLLIElement | null>(),
    onCollapseNavbar: handleCollapseNavbarMenu,
    active: selected,
  }));

  // animation active navlink scroll
  const handleScroll = () => {
    const pageYOffset = window.scrollY;
    let newActiveSection = null;

    sectionsZone.current.forEach((section: any) => {
      const sectionOffsetTop = section.offsetTop;
      const sectionHeight = section.offsetHeight;

      if (
        pageYOffset >= sectionOffsetTop - 550 &&
        pageYOffset < sectionOffsetTop - 550 + sectionHeight
      ) {
        newActiveSection = section.id;
      }
    });

    setSelected(newActiveSection);
  };
  useEffect(() => {
    sectionsZone.current = document.querySelectorAll("[data-section]");
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [handleScroll]);
  return (
    <div>
      {children}
      <Navbar data={allPages} />
      <RegularList
        items={allPages}
        resourceName="page"
        itemComponent={SectionItem}
      />
    </div>
  );
}

type NavbarProps = {
  data: any;
};
function Navbar({ data }: NavbarProps) {
  return (
    <nav className="sticky bg-accent top-0">
      <ul>
        <RegularList
          items={data}
          resourceName="page"
          itemComponent={NavbarListItem}
        />
      </ul>
    </nav>
  );
}

interface NavbarListItemProps {
  page: any;
  active: string;
}
function NavbarListItem({ page }: NavbarListItemProps) {
  const { title, slug, nodeRef, onCollapseNavbar, active } = page;
  return (
    <li
      className={`mt-0 font-medium text-2xl hover:opacity-80 py-6 md:text-xl lg:px-12 lg:py-2 lg:text-l lg:rounded-md ${
        active === slug ? "active" : ""
      }`}
      ref={nodeRef}
    >
      <a href={`/#${slug}`} onClick={onCollapseNavbar}>
        {title}
      </a>
    </li>
  );
}

function SectionItem({ page }: { page: any }) {
  const { description } = page;
  return (
    <section data-section id={page.slug} className="min-h-screen">
      {description}
    </section>
  );
}
