import { useState } from "react";

import { FieldProps, useField } from "@formiz/core";
import { FormGroup, FormGroupProps } from "./FormGroup";
import Spin from "../assets/Spin";

type Value = string;

export type FieldInputProps<FormattedValue = Value> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps & {
    isLoading?: boolean;
  };

export const FieldInput = <FormattedValue = Value,>(
  props: FieldInputProps<FormattedValue>
) => {
  const {
    errorMessage,
    id,
    isValid,
    isTouched,
    isRequired,
    isValidating,
    isProcessing,
    isDebouncing,
    isReady,
    isPristine,
    setValue,
    value,
    setIsTouched,
    shouldDisplayError,
    otherProps: {
      children,
      label,
      type,
      placeholder,
      helper,
      size,
      autoFocus = false,
      isLoading = false,
      ...rest
    },
  } = useField(props);
  const [showPassword, setShowPassword] = useState(false);

  const formGroupProps = {
    errorMessage,
    helper,
    id,
    isRequired,
    label,
    showError: shouldDisplayError,
    ...rest,
  };

  return (
    <FormGroup {...formGroupProps}>
      <>
        <input
          className={`w-full p-4 mb-4 rounded-xl border-2 text-base border-dark ${
            !isValid && isTouched && "border-accent"
          } ${!isProcessing && isValid && "border-green200"}`}
          type={showPassword ? "text" : type || "text"}
          id={id}
          value={value ?? ""}
          onChange={(e) => setValue(e.target.value)}
          onFocus={() => setIsTouched(false)}
          onBlur={() => setIsTouched(true)}
          placeholder={placeholder}
          autoFocus={autoFocus}
        />
        <div>
          {!isReady && "🚧"}
          {isReady && isProcessing && "⏳"}
          {/* {!isValid && isTouched && "❌"} */}
          {/* {!isProcessing && isValid && "✅"} */}
        </div>
        <div>
          {isDebouncing && "✋🏼"}
          {isValidating || isLoading ? <Spin /> : isPristine && "✨"}
        </div>
        <button className="hidden" onClick={() => setShowPassword(true)}>
          show password
        </button>
      </>
      {children}
    </FormGroup>
  );
};
