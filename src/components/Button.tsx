import { ReactNode, useContext } from "react";
import { ThemeContext } from "../routes/root";

type ButtonPops = {
  bgBorder: string;
  handleClick: () => void;
  children: ReactNode;
};
export function Button({ bgBorder, handleClick, children }: ButtonPops) {
  const theme = useContext(ThemeContext);

  const buttonStyle =
    theme === "light"
      ? "bg-orange text-white border-orange md:hover:bg-transparent md:hover:text-orange"
      : "bg-orangeDark border-orangeDark md:hover:bg-transparent";

  const rectangleStyle = bgBorder;

  const rectangleBarStyle = theme === "light" ? "bg-orange" : "bg-orangeDark";

  return (
    <>
      <button
        className={`relative py-2 px-12 text-base border-2 ${buttonStyle} md:py-3 md:text-xl`}
        onClick={handleClick}
      >
        {children}
        <span
          className={`absolute top-[-10.5px] left-[-10.5px] w-[20px] h-[20px] flex items-start rotate-[135deg] ${rectangleStyle} `}
        >
          <span className={`h-[2px] w-[100%] ${rectangleBarStyle} `}></span>
        </span>
        <span
          className={`absolute bottom-[-10.5px] right-[-10.5px] w-[20px] h-[20px] flex items-end rotate-[135deg] ${rectangleStyle}`}
        >
          <span className={`h-[2px] w-[100%] ${rectangleBarStyle} `}></span>
        </span>
      </button>
    </>
  );
}
