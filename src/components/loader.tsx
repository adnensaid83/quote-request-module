import loader from "../assets/loader.gif";
const Loader = () => {
  return (
    <div className="fixed top-0 h-screen w-screen bg-green flex items-center justify-center font-bold">
      <div className="w-24">
        <img src={loader} alt="loader" />
      </div>
    </div>
  );
};

export default Loader;
