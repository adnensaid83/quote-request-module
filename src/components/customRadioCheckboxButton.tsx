import { FieldProps, useField } from "@formiz/core";
import { ReactNode } from "react";
import { FormGroup, FormGroupProps } from "./FormGroup";

export default function CustomRadioCheckboxButton({
  type,
  label,
  name,
  value,
}: {
  type: string;
  name: string;
  value: string;
  label: string;
}) {
  return (
    <>
      <input
        type={type}
        name={name}
        id={value + name /* id unique conflict forfait et lang */}
        value={value ?? ""}
        className="sr-only"
      />
      <label
        htmlFor={value + name}
        className={`block w-full p-4 rounded-xl text-l cursor-pointer`}
      >
        {label}
      </label>
    </>
  );
}

type MyFieldProps<FormattedValue> = FieldProps<string, FormattedValue>;

export const MyField = <FormattedValue = string,>(
  props: MyFieldProps<FormattedValue>
) => {
  const { value, setValue, isValid, errorMessage } = useField(props);
  return (
    <>
      <input value={value ?? ""} onChange={(e) => setValue(e.target.value)} />
      {
        !isValid && <p>{errorMessage}</p> // Display error message
      }
    </>
  );
};

// Select

export type SelectOption = {
  label?: ReactNode;
  value: string | number;
};

type Value = SelectOption["value"];

export type FieldSelectProps<FormattedValue> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps & {
    isLoading?: boolean;
    options: SelectOption[];
  };

export const FieldSelect = <FormattedValue = Value,>(
  props: FieldSelectProps<FormattedValue>
) => {
  const {
    errorMessage,
    id,
    isRequired,
    setValue,
    value,
    shouldDisplayError,
    setIsTouched,
    otherProps: {
      children,
      label,
      placeholder,
      helper,
      size,
      autoFocus = false,
      isLoading = false,
      options,
      ...rest
    },
  } = useField(props);

  const formGroupProps = {
    errorMessage,
    helper,
    id,
    isRequired,
    label,
    showError: shouldDisplayError,
    ...rest,
  };

  return (
    <FormGroup {...formGroupProps}>
      <select
        id={id}
        value={value ?? ""}
        onChange={(e) => setValue(e.target.value)}
        onFocus={() => setIsTouched(false)}
        onBlur={() => setIsTouched(true)}
        placeholder={placeholder}
        autoFocus={autoFocus}
      >
        {options?.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label ?? option.value}
          </option>
        ))}
      </select>
      {children}
    </FormGroup>
  );
};
