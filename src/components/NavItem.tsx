import { NavLink } from "react-router-dom";
import { ServiceI } from "../interfaces/Service.interface";
import ArrowRight from "./ArrowRight";
import Markdown from "react-markdown";

type NavItemProps = {
  service: ServiceI;
  index: number;
};
export function NavItem({ service }: NavItemProps) {
  const { tag, title, description } = service;
  return (
    <NavLink
      to={`/services/${service.slug}`}
      className={`border-[2px] even:border-b-0 even:border-t-0 lg:border-b-0 lg:even:border-t-[2px] lg:even:border-l-0 `}
    >
      <div className="flex flex-col gap-8 py-12 px-12 cursor-pointer transition duration-150 ease-out hover:ease-in group/item hover:bg-green300 hover:text-white hover:scale-101">
        <p className="text-base font-light uppercase"> {tag?.title} </p>
        <h2 className="text-3xl font-normal md:text-4.4xl">{title}</h2>
        <div className="font-light line-clamp-3 prose prose-xl text-dark prose-p:text-dark prose-p:text-xl prose-p:leading-normal group-hover/item:prose-p:text-white">
          <Markdown>{description}</Markdown>
        </div>
        <button className="w-20 h-20 origin-top-center group-hover/item:animate-bounce-x">
          <ArrowRight />
        </button>
      </div>
    </NavLink>
  );
}
