import { ReactNode } from "react";

type SplitScreenProps = {
  children: ReactNode[];
  leftStyle: string;
  rightStyle: string;
  className: string;
};

export const SplitScreen = ({
  children,
  leftStyle,
  rightStyle,
  className,
}: SplitScreenProps) => {
  const [left, right] = children;

  return (
    <div className={className}>
      <div className={leftStyle}>{left}</div>
      <div className={rightStyle}>{right}</div>
    </div>
  );
};
