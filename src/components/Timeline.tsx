import { ReactNode } from "react";
import { Container } from "./Container";

export function TimeLine({ children }: { children: ReactNode }) {
  return (
    <div className="relative">
      <Container>
        <ul>{children}</ul>
      </Container>
    </div>
  );
}
