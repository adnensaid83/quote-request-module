import { ReactNode, useContext } from "react";
import { ThemeContext } from "../routes/root";

export function TitleSection({ children }: { children: ReactNode }) {
  return (
    <h2 className="text-2xl font-semibold text-center flex items-center justify-center gap-2 md:text-3xl md:gap-8">
      <Underline /> {children} <Underline />
    </h2>
  );
}

export function Underline() {
  const theme = useContext(ThemeContext);
  const color = theme === "light" ? "bg-orange" : "bg-orange";
  return (
    <span
      className={` block w-[50px] h-[4px] md:w-[80px] md:h-[3px] ${color} `}
    ></span>
  );
}
