import { FormGroupProps } from "./FormGroup";
import { FieldProps, useField } from "@formiz/core";
import { ReactNode } from "react";
import { FormGroupOption } from "./FormGroupOption";

export type CheckboxOption = {
  label?: ReactNode;
  value: string;
};

type Value = CheckboxOption["value"];

export type FieldCheckboxProps<FormattedValue> = FieldProps<
  Value[],
  FormattedValue
> &
  FormGroupProps & {
    options: CheckboxOption[];
  };

export const FieldCheckbox = <FormattedValue = Value[],>(
  props: FieldCheckboxProps<FormattedValue>
) => {
  const {
    errorMessage,
    id,
    isRequired,
    setValue,
    value = [],
    shouldDisplayError,
    setIsTouched,
    otherProps: { options, ...rest },
  } = useField(props);
  const handleChange = (selectedValue: Value) => {
    const updatedValue = value || [];

    const isSelected = updatedValue.includes(selectedValue);

    let newValue: Value[];
    if (isSelected) {
      newValue = updatedValue.filter((val) => val !== selectedValue);
    } else {
      newValue = [...updatedValue, selectedValue];
    }
    setValue(newValue);
    setIsTouched(true);
  };
  const formGroupProps = {
    errorMessage,
    id,
    isRequired,
    showError: shouldDisplayError,
    ...rest,
  };
  return (
    <FormGroupOption {...formGroupProps}>
      {options.map((option) => (
        <div key={`${id}-${option.value}`}>
          <input
            id={`${id}-${option.value}`}
            type="checkbox"
            value={option.value ?? ""}
            checked={Array.isArray(value) && value.includes(option.value)}
            onChange={() => handleChange(option.value)}
            className="sr-only"
          />
          <label
            htmlFor={`${id}-${option.value}`}
            className="block w-full p-4 rounded-xl text-l cursor-pointer"
          >
            {option.label ?? option.value}
          </label>
        </div>
      ))}
    </FormGroupOption>
  );
};
