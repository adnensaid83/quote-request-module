import { Link, useLocation } from "react-router-dom";

export default function Breadcrump() {
  const location = useLocation();
  let currentLink = "";
  const crumbs = location.pathname
    .split("/")
    .filter((crumb) => crumb !== "")
    .map((crumb, index, array) => {
      currentLink += `/${crumb}`;
      const isLast = index === array.length - 1;
      const capitalizedCrumb = crumb.charAt(0).toUpperCase() + crumb.slice(1);
      return (
        <div className="whitespace-nowrap" key={crumb}>
          <Link
            to={currentLink}
            className=" hover:underline hover:text-orange "
          >
            {capitalizedCrumb}
          </Link>
          {!isLast && <span className="px-2 md:px-6"> &gt; </span>}
        </div>
      );
    });
  return (
    <div className="flex text-sm md:text-sm py-6">
      <div className="flex overflow-x-scroll overscroll-auto no-scrollbar">
        <Link to="/" className="flex items-center">
          <span className="hover:underline hover:text-orange">Accueil</span>
        </Link>
        {!!crumbs.length && <span className="px-2 md:px-6"> &gt; </span>}
        {crumbs}
      </div>
    </div>
  );
}
