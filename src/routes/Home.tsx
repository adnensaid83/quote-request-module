import { NavLink } from "react-router-dom";

const Home = () => {
  return (
    <div className="min-h-screen flex flex-col items-center justify-center">
      <h1 className="mb-12">Module de demande de devis</h1>
      <NavLink
        className={
          "bg-orange text-white px-8 py-4 rounded-md text-l border-orange border-2 transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-orange"
        }
        to="/services"
      >
        Commencer
      </NavLink>
    </div>
  );
};

export default Home;
