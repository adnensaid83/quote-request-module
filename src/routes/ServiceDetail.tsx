import { useParams } from "react-router-dom";
import Loader from "../components/loader";
import QuoteRequest from "./QuoteRequest";
import gql from "graphql-tag";
import { useQuery } from "@apollo/client";

const ServiceDetail = () => {
  const { slug } = useParams();
  const { loading, error, data } = useQuery(GET_SERVICE_BY_SLUG, {
    variables: { slug },
  });
  if (loading) return <Loader />;
  if (error) return <div>Error:</div>;
  const selectedService = data.allServices[0];
  return <QuoteRequest service={selectedService} />;
};

export default ServiceDetail;

const GET_SERVICE_BY_SLUG = gql`
  query GetServiceBySlug($slug: String!) {
    allServices(filter: { slug: { eq: $slug } }) {
      id
      slug
      title
      question {
        name
        order
        title
        option {
          order
          title
          label
          value
        }
        choice {
          name
        }
      }
    }
  }
`;
