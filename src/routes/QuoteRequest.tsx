import { useState } from "react";
import ArrowLeft from "../components/ArrowLeft";
import ArrowRight from "../components/ArrowRight";
import "swiper/css";
import { Formiz, FormizStep, useForm } from "@formiz/core";
import { isEmail, isNumber } from "@formiz/validations";
import { FieldRadio } from "../components/RadioOption";
import { FieldInput } from "../components/FieldInput";
import { FieldCheckbox } from "../components/CheckboxOption";
import Spin from "../assets/Spin";
import { FieldTextarea } from "../components/FieldTextarea";
import Layout from "../components/Layout";
import { QuestionI, ServiceI } from "../interfaces/Service.interface";
import { LogLevel, buildClient } from "@datocms/cma-client-browser";
import { FieldPickIdenticalImages } from "../components/FieldPickIdenticalImages";
import { useNavigate } from "react-router-dom";
import X from "../assets/X";
const lastQuestion: QuestionI[] = [
  {
    name: "last",
    title: "Dernière étape avant l'estimation de prix:",
    choice: {
      name: "",
    },
    option: [
      {
        label: "Nom et Prénom",
        value: "fullname",
        order: "1",
      },
      {
        label: "Adresse mail",
        value: "email",
        order: "2",
      },
      {
        label: "Numéro de téléphone",
        value: "phone",
        order: "3",
      },
      {
        label: "Code postal",
        value: "postal",
        order: "4",
      },
      {
        label: "Présentez brièvement votre projet",
        value: "description",
        order: "5",
      },
    ],
  },
];
const captcha: QuestionI[] = [
  {
    name: "twoPictures",
    title: "Sélectionnez deux images identiques",
    choice: {
      name: "",
    },
    option: [
      {
        label: "",
        value: "https://source.unsplash.com/WLUHO9A_xik/200x200",
        order: "1",
      },
      {
        label: "",
        value: "https://source.unsplash.com/WLUHO9A_xik/200x200",
        order: "2",
      },
      {
        label: "",
        value: "https://source.unsplash.com/2Mr4lBxHpcg/200x200",
        order: "3",
      },
      {
        label: "",
        value: "https://source.unsplash.com/AjvgNTbyuG8/200x200",
        order: "4",
      },
      {
        label: "",
        value: "https://source.unsplash.com/hyGX23RpLTU/200x200",
        order: "5",
      },
      {
        label: "",
        value: "https://source.unsplash.com/4nulm-JUYFo/200x200",
        order: "6",
      },
      {
        label: "",
        value: "https://source.unsplash.com/MQ4eKnHtOUg/200x200",
        order: "7",
      },
      {
        label: "",
        value: "https://source.unsplash.com/pzMP-RGJ7mY/200x200",
        order: "8",
      },
      {
        label: "",
        value: "https://source.unsplash.com/httxBNGKapo/200x200",
        order: "9",
      },
    ],
  },
];
const fakeDelay = (delay = 500) => new Promise((r) => setTimeout(r, delay));
type OptionDevis = {
  question: string;
  response: string[] | string;
};
type FormValues = any;
export default function QuoteRequest({ service }: { service: ServiceI }) {
  const [status, setStatus] = useState("idle");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const navigate = useNavigate();
  async function createDevisRecord({
    dataForm,
  }: {
    dataForm: {
      fullname: string;
      email: string;
      phone: string;
      postal: string;
      description: string;
      details: string;
    };
  }) {
    const client = buildClient({
      apiToken: import.meta.env.VITE_API_TOKEN,
      logLevel: LogLevel.BASIC,
    });
    try {
      await client.items.create({
        item_type: { type: "item_type", id: import.meta.env.VITE_ESTIMATE_ID },
        ...dataForm,
      });
      //console.log("Nouveau devis créé:", newDevis);
      setStatus("success");
      setShowSuccessAlert(true);
    } catch (error) {
      console.error('Erreur lors de la création du modèle "devis" :', error);
    }
  }

  const questionList = service?.question;
  type AccI = {
    [key: string]: string | string[];
  };
  const getInitialValues = () => {
    const initialValues = questionList?.reduce(
      (acc: AccI, question: QuestionI) => {
        if (question.choice.name === "unique") {
          acc[question.name] = "";
        } else if (question.choice.name === "multiple") {
          acc[question.name] = [];
        }
        return acc;
      },
      {}
    );
    return {
      ...initialValues,
      fullname: "",
      email: "",
      phone: "",
      postal: "",
      description: "",
    };
  };

  const combinedQuestions = questionList?.concat([...captcha, ...lastQuestion]);
  const handleSubmitStep = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (
      !form.currentStep ||
      !form.currentStep?.isValid ||
      !form.currentStep?.name
    ) {
      form.submitStep();
      return;
    }

    setStatus("loading");
    //console.log(`Submitting ${form.currentStep?.name}...`); // eslint-disable-line no-console
    await fakeDelay();
    setStatus("success");
    form.submitStep();
  };
  const form = useForm<FormValues>({
    ready: true,
    initialValues: getInitialValues(),
    onValidSubmit: async (values, form) => {
      let details: OptionDevis[] = [];

      const fieldNames = questionList?.map((i: QuestionI) => i.name);
      fieldNames?.forEach((fieldName: string) => {
        const value = values[fieldName as keyof FormValues];
        if (value) {
          const question = questionList?.filter((q) => q.name === fieldName)[0];
          if (question) {
            const title = question.title || "default";
            if (Array.isArray(value)) {
              details.push({
                question: title,
                response: [...value],
              });
            } else {
              details.push({
                question: title,
                response: value,
              });
            }
          }
        }
      });

      const dataForm = {
        fullname: values.fullname,
        email: values.email,
        phone: values.phone,
        postal: values.postal,
        description: values.description,
        details: JSON.stringify(details),
      };
      setStatus("loading");
      createDevisRecord({ dataForm });
      form.setErrors({
        name: "You can display an error after an API call",
      });
      const stepWithError = form.getStepByFieldName("name");
      if (stepWithError) {
        form.goToStep(stepWithError.name);
      }
    },
    //onValuesChange: () => console.log("values change"),
    //onValid: () => console.log("onValid"),
    //onInvalid: () => console.log("onInvalid"),
  });
  const isLoading = status === "loading" || form.isValidating;
  return (
    <Layout title={service?.title ?? ""} tag="Demande de devis">
      <div className="py-8 lg:py-32 relative">
        <div className="max-w-[800px] mx-auto">
          <Formiz connect={form}>
            <form noValidate onSubmit={handleSubmitStep} className="relative">
              {combinedQuestions?.map((question) => (
                <FormizStep
                  name={question.name}
                  key={question.name + question.title}
                >
                  {question.name === "last" ? (
                    <>
                      <div className="flex items-center gap-4 mb-12">
                        <span className="flex items-center justify-center text-xl border-2 border-orange rounded-full text-white bg-orange w-16 h-16 shrink-0">
                          {(form.currentStep?.index ?? 0) + 1}
                        </span>
                        <p className="mb-0 uppercase font-medium text-orange">
                          {question.title}
                        </p>
                      </div>
                      <div
                        className={`grid grid-cols-1 gap-8 mb-12 ${
                          length && length <= 2
                            ? "md:grid-cols-1"
                            : "md:grid-cols-2"
                        }`}
                      >
                        {question.option.map((o, i) =>
                          o.value === "email" ? (
                            <FieldInput
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                              validations={[
                                {
                                  handler: isEmail(),
                                  message: "Not a valid email",
                                },
                              ]}
                            />
                          ) : o.value === "description" ? (
                            <FieldTextarea
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                            />
                          ) : o.value === "fullname" ? (
                            <FieldInput
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                              formatValue={(val) => (val || "").trim()}
                            />
                          ) : (
                            <FieldInput
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                              formatValue={(val) => (val || "").trim()}
                              validations={[
                                {
                                  handler: isNumber(),
                                  message: "Not a valid input",
                                },
                              ]}
                            />
                          )
                        )}
                      </div>
                    </>
                  ) : question.name === "twoPictures" ? (
                    <>
                      <FieldPickIdenticalImages
                        name={question.name}
                        order={(form.currentStep?.index ?? 0) + 1}
                        label="SÉLECTIONNEZ DEUX IMAGES IDENTIQUES"
                        helper=""
                        options={question.option.map((q) => q.value)}
                      />
                    </>
                  ) : question.choice.name === "unique" ? (
                    <FieldRadio
                      name={question.name}
                      order={(form.currentStep?.index ?? 0) + 1}
                      label={question.title}
                      options={question.option}
                      required="Ce champ est obligatoire "
                    />
                  ) : (
                    <FieldCheckbox
                      name={question.name}
                      order={(form.currentStep?.index ?? 0) + 1}
                      label={question.title}
                      options={question.option}
                    />
                  )}
                </FormizStep>
              ))}
              {!!form.steps?.length && (
                <div className="grid grid-cols2 justify-between items-center">
                  {!form.isFirstStep && (
                    <button
                      className="col-start-1 w-[48px] h-[30px] drop-shadow-md border-[3px] border-green200 rounded-md text-green200 disabled:opacity-0 md:w-[50px] md:h-[35px]"
                      onClick={(e) => {
                        e.preventDefault();
                        form.goToPreviousStep();
                      }}
                    >
                      <ArrowLeft />
                    </button>
                  )}
                  {/*                 <div>
                  Step {(form.currentStep?.index ?? 0) + 1} /{" "}
                  {form.steps.length}
                </div> */}

                  {form.isLastStep ? (
                    <>
                      <button
                        type="submit"
                        disabled={
                          (form.isLastStep
                            ? !form.isValid
                            : !form.isStepValid) && form.isStepSubmitted
                        }
                        className={
                          "col-start-2 bg-orange text-white px-8 py-4 rounded-md text-l border-orange border-2 transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-orange"
                        }
                      >
                        {isLoading ? (
                          <div className="flex items-center gap-2">
                            <Spin />
                            Envoyez la demande de devis
                          </div>
                        ) : (
                          <div>Envoyer la demande de devis</div>
                        )}
                      </button>
                    </>
                  ) : (
                    <button
                      type="submit"
                      disabled={
                        (form.isLastStep ? !form.isValid : !form.isStepValid) &&
                        form.isStepSubmitted
                      }
                      className={
                        "col-start-2 w-[48px] h-[30px] flex justify-center items-center drop-shadow-md border-[3px] border-green200 rounded-md text-green200 disabled:opacity-0 md:w-[50px] md:h-[35px]"
                      }
                    >
                      {!isLoading ? <ArrowRight /> : <Spin />}
                    </button>
                  )}
                </div>
              )}
            </form>
          </Formiz>
        </div>
      </div>
      {showSuccessAlert && (
        <div className="fixed w-full h-full top-0 left-0 bg-green200  flex items-center justify-center">
          <div
            className="relative bg-white text-dark rounded-xl m-4"
            role="alert"
          >
            <p className="m-0 text-2xl px-28 py-12 text-center md:text-3xl">
              Votre demande de devis a été envoyée avec succès !
            </p>
            <span
              className="block w-16 absolute top-0 right-0 cursor-pointer"
              onClick={() => {
                navigate("/");
                setShowSuccessAlert(false);
              }}
            >
              <X />
            </span>
          </div>
        </div>
      )}
    </Layout>
  );
}
