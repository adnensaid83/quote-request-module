import Layout from "../components/Layout";
import { RegularList } from "../components/RegularList";
import { Underline } from "../components/TitleSection";
import Loader from "../components/loader";
import gql from "graphql-tag";
import { useQuery } from "@apollo/client";
import { NavItem } from "../components/NavItem";
import Markdown from "react-markdown";

export default function ServicesPage() {
  const { loading, error, data } = useQuery(SERVICES_QUERY, {
    variables: { limit: 4 },
  });
  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <Layout title="Services">
      <h2 className="flex flex-col gap-6 pb-12 text-3xl lg:text-4xl lg:text-start lg:items-start">
        Services
        <Underline />
      </h2>
      {/*       <div className="max-w-[800px] mb-12 prose prose-xl prose-p:text-dark prose-p:text-xl prose-p:leading-normal">
        <Markdown>{data.servicePage.description}</Markdown>
      </div> */}
      <ul
        className={`grid grid-cols-1 border-b-[2px] border-dark transition ease-out duration-400 md:grid-cols-1 md:group/item lg:grid-cols-2`}
      >
        <RegularList
          items={data.allServices}
          resourceName="service"
          itemComponent={NavItem}
        />
      </ul>
    </Layout>
  );
}
const SERVICES_QUERY = gql`
  query GetServicePage {
    allServices(orderBy: order_ASC) {
      id
      slug
      title
    }
  }
`;

/* const SERVICES_QUERY = gql`
  query GetServicePage {
    servicePage {
      id
      name
      slug
      bigTitle
      smallTitle
      description
      services {
        id
        order
        slug
        title
        description
      }
    }
  }
`; */
