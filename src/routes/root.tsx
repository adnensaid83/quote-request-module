import { createContext } from "react";
import { Outlet } from "react-router-dom";
import { Footer } from "../components/Footer";
import ScrollToTop from "../components/ScrollToTop";
export const ThemeContext = createContext<any>(null);

export default function Root() {
  return (
    <div className={`light`}>
      <ScrollToTop>
        <div id="detail">
          <Outlet />
        </div>
      </ScrollToTop>
      <Footer />
    </div>
  );
}
