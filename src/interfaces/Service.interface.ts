export interface ServiceI {
  id?: string;
  title: string;
  slug: string;
  description: string;
  tag?: { title: string };
  question?: QuestionI[];
}

export interface QuestionI {
  name: string;
  order?: number;
  title: string;
  option: OptionI[];
  choice: {
    name: string;
  };
}

export interface OptionI {
  label: string;
  value: string;
  order: string;
}
